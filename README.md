A single page application using React as frontend and spring boot as backend.

This specific one is React-frontend (client) – Consumes REST API. Check out [Its Backend Implementation](https://gitlab.com/MakariKevin/spring-boot-with-reactjs-h2-database)

## Tools and Technologies Used
- React JS
- Axios
- Bootstrap
- H2 Database
- VS Code

## Client-Server Architecture

![Screenshot 2022-10-25 at 09.48.38.png](./Screenshot 2022-10-25 at 09.48.38.png)

## Output

![Screenshot 2022-10-25 at 09.40.52.png](./Screenshot 2022-10-25 at 09.40.52.png)
